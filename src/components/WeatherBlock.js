import React, {useEffect, useState} from 'react';
import axios from "axios";
import CitySearch from "./CitySearch";
import WeatherInfoBlock from "./WeatherInfoBlock";
import {connect} from "react-redux";
import {setCity} from "../redux/actions/cityActions";

function WeatherBlock({city, setCity}) {

    const [weatherData, setWeatherData] = useState({});

    console.log(city)

    useEffect(() => {
        axios({url: `https://api.weatherapi.com/v1/current.json?key=42d4ddf34e6f43078aa101857210209&q=${city.name}&aqi=no`})
            .then((response) => {
                console.log(response.data)
                setWeatherData(response.data)
            }).catch((err) => {
                console.error(err)
            })

    }, [city.name])

    return (
        <>
            <CitySearch setCity={setCity}/>
            {Object.keys(weatherData).length ?
                <WeatherInfoBlock weather={weatherData.current}/>
                :
                <div>Enter city!</div>
            }
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        city: state.city
    }
}

const mapDispatchToProps = {
    setCity
}

export default connect(mapStateToProps, mapDispatchToProps)(WeatherBlock)
