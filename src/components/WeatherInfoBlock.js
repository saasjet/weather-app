import React from "react";

export default function WeatherInfoBlock({weather}){
    return (
        <>
            <div>Temp: {weather.temp_c}</div>
            <div>Feels like: {weather.feelslike_c}</div>
            <div>Humidity: {weather.humidity}</div>
            <div>Wind speed: {weather.wind_kph}</div>
        </>
    )
}