import React from 'react';

export default function CitySearch({setCity}){
    return (
        <input type="text" onChange={(e)=>{console.log(e.target.value); setCity({name: e.target.value})}} />
    )
}