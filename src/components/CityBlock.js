import React from "react";
import {connect} from "react-redux";

function CityBlock({city}){
    return (
        <div>
            {city?.name ?
                city.name
                :
                <div>No data available</div>
            }
        </div>
    )
}

function mapStateToProps(state) {
    return {
        city: state.city
    }
}

export default connect(mapStateToProps, null)(CityBlock)