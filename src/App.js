import './App.css';
import store from "./redux/store";
import WeatherBlock from "./components/WeatherBlock";
import {Provider} from "react-redux";
import CityBlock from "./components/CityBlock";

function App() {
  return (
    <div className="App">
      <Provider store={store}>
          <WeatherBlock/>
          <CityBlock />
      </Provider>
    </div>
  );
}

export default App;
