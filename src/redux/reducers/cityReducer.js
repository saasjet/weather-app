import {SET_CITY} from "../types";

const initialState = {};

export const cityReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_CITY:
            return {...state, ...action.payload.city}
        default: return state;
    }
}