import {cityReducer} from "./cityReducer";
import {combineReducers} from "redux";

const rootReducer = combineReducers({
    city: cityReducer
})

export default rootReducer;