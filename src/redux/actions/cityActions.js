import {SET_CITY} from "../types";

export const setCity = (city) => (
    {type: SET_CITY, payload: {city: city}}
);